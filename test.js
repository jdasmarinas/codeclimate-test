async function fetchData(urls) {
    for (let i = 0; i < urls.length; i++) {
        // This will trigger the `no-await-in-loop` ESLint rule
        const response = await fetch(urls[i]);
        const data = await response.json();
        console.log(data);
    }
}

const urls = [
    'https://api.example.com/data1',
    'https://api.example.com/data2',
    'https://api.example.com/data3'
];

fetchData(urls);